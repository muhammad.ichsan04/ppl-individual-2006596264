from django.shortcuts import render

# Create your views here.
def index(request):
    context = {
        'name' : 'Muhammad Ichsan Khairullah',
        'npm' : 2006596264
    }
    return render(request, 'index.html', context)